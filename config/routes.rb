# frozen_string_literal: true

Rails.application.routes.draw do
  resources :gps_coordinates, only: %i[create]
  resources :routes, only: %i[index]
  post "search", to: "search_buses#search"
  post "search_coordinates", to: "search_coordinates#search"
end

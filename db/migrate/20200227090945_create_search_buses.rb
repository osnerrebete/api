# frozen_string_literal: true

class CreateSearchBuses < ActiveRecord::Migration[5.2]
  def change
    create_view :search_buses
  end
end

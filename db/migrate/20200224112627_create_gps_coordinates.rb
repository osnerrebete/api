# frozen_string_literal: true

class CreateGpsCoordinates < ActiveRecord::Migration[5.2]
  def change
    create_table :gps_coordinates do |t|
      t.references :bus, foreign_key: true
      t.st_point :lonlat, geographic: true
      t.timestamps
    end
    add_index :gps_coordinates, :lonlat
  end
end

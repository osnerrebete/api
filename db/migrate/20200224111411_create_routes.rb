# frozen_string_literal: true

class CreateRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.string :start_name
      t.string :end_name
      t.st_point :start_point, geographic: true
      t.st_point :end_point, geographic: true
      t.st_polygon :coordinates, geographic: true
      t.timestamps
    end
  end
end

class CreateJoinTableBusesRoutes < ActiveRecord::Migration[5.2]
  def change
    create_join_table :buses, :routes do |t|
      t.index [:bus_id, :route_id]
      t.index [:route_id, :bus_id]
    end
  end
end

# frozen_string_literal: true

class CreateBuses < ActiveRecord::Migration[5.2]
  def change
    create_table :buses do |t|
      t.string :plate
      t.string :gps_serial

      t.timestamps
    end
    add_index :buses, :gps_serial
    add_index :buses, :plate
  end
end

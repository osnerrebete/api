select 
	distinct on (c_start.id)
	c_start.id,
	buses.plate,
	routes.id as route_id,
	c_start.created_at as start_date,
	c_end.created_at AS end_date,
	case
		when (extract('epoch' from now() - c_end.created_at) / 60) > 15 and ST_DistanceSphere(c_end.lonlat::geometry, routes.end_point::geometry) > 100 then 2
		when ST_DistanceSphere(c_end.lonlat::geometry, routes.end_point::geometry) <= 100 then 1
		when ST_DistanceSphere(c_start.lonlat::geometry, routes.start_point::geometry) >= 0 then 0
		else 3 end
		as status
	from
		gps_coordinates c_start
	inner join buses on
		c_start.bus_id = buses.id
	inner join buses_routes on buses_routes.bus_id = c_start.bus_id
	inner join routes on
		buses_routes.route_id = routes.id
	inner join gps_coordinates as c_end on c_start.bus_id = c_end.bus_id
	where ST_DistanceSphere(c_end.lonlat::geometry, routes.end_point::geometry) in
			(select min(ST_DistanceSphere(gc.lonlat::geometry, routes.end_point::geometry)) from gps_coordinates gc where gc.bus_id = buses.id) 
			and ST_DistanceSphere(c_start.lonlat::geometry, routes.start_point::geometry) in
			(select min(ST_DistanceSphere(gc.lonlat::geometry, routes.start_point::geometry)) from gps_coordinates gc where gc.bus_id = buses.id)
			and c_end.id >= c_start.id
	order by
		c_start.id asc
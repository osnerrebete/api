 select
	distinct on
	(c_current.id) c_current.id,
	buses.plate,
	c_current.lonlat,
	c_current.created_at as current_date,
	(extract('epoch'
	from
		 c_current.created_at - c_next.created_at) / 60) lapse
	from
		gps_coordinates c_current
	inner join buses on
		c_current.bus_id = buses.id
	inner join buses_routes on
		buses_routes.bus_id = c_current.bus_id
	inner join routes on
		buses_routes.route_id = routes.id
	inner join gps_coordinates as c_next on
		c_current.bus_id = c_next.bus_id
	where
		ST_DistanceSphere(c_next.lonlat::geometry,
		c_current.lonlat::geometry) in (
		select
			max(ST_DistanceSphere(c_current.lonlat::geometry, gc.lonlat::geometry))
		from
			gps_coordinates gc
		where
			gc.bus_id = buses.id and ST_DistanceSphere(c_current.lonlat::geometry,
		gc.lonlat::geometry) <= 100 ) and c_next.id >= c_current.id
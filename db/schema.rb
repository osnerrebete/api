# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_28_104339) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "buses", force: :cascade do |t|
    t.string "plate"
    t.string "gps_serial"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gps_serial"], name: "index_buses_on_gps_serial"
    t.index ["plate"], name: "index_buses_on_plate"
  end

  create_table "buses_routes", id: false, force: :cascade do |t|
    t.bigint "bus_id", null: false
    t.bigint "route_id", null: false
    t.index ["bus_id", "route_id"], name: "index_buses_routes_on_bus_id_and_route_id"
    t.index ["route_id", "bus_id"], name: "index_buses_routes_on_route_id_and_bus_id"
  end

  create_table "gps_coordinates", force: :cascade do |t|
    t.bigint "bus_id"
    t.geography "lonlat", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bus_id"], name: "index_gps_coordinates_on_bus_id"
    t.index ["lonlat"], name: "index_gps_coordinates_on_lonlat"
  end

  create_table "routes", force: :cascade do |t|
    t.string "start_name"
    t.string "end_name"
    t.geography "start_point", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.geography "end_point", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.geography "coordinates", limit: {:srid=>4326, :type=>"st_polygon", :geographic=>true}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "gps_coordinates", "buses"

  create_view "search_buses", sql_definition: <<-SQL
      SELECT DISTINCT ON (c_start.id) c_start.id,
      buses.plate,
      routes.id AS route_id,
      c_start.created_at AS start_date,
      c_end.created_at AS end_date,
          CASE
              WHEN (((date_part('epoch'::text, (now() - (c_end.created_at)::timestamp with time zone)) / (60)::double precision) > (15)::double precision) AND (st_distancesphere((c_end.lonlat)::geometry, (routes.end_point)::geometry) > (100)::double precision)) THEN 2
              WHEN (st_distancesphere((c_end.lonlat)::geometry, (routes.end_point)::geometry) <= (100)::double precision) THEN 1
              WHEN (st_distancesphere((c_start.lonlat)::geometry, (routes.start_point)::geometry) >= (0)::double precision) THEN 0
              ELSE 3
          END AS status
     FROM ((((gps_coordinates c_start
       JOIN buses ON ((c_start.bus_id = buses.id)))
       JOIN buses_routes ON ((buses_routes.bus_id = c_start.bus_id)))
       JOIN routes ON ((buses_routes.route_id = routes.id)))
       JOIN gps_coordinates c_end ON ((c_start.bus_id = c_end.bus_id)))
    WHERE ((st_distancesphere((c_end.lonlat)::geometry, (routes.end_point)::geometry) IN ( SELECT min(st_distancesphere((gc.lonlat)::geometry, (routes.end_point)::geometry)) AS min
             FROM gps_coordinates gc
            WHERE (gc.bus_id = buses.id))) AND (st_distancesphere((c_start.lonlat)::geometry, (routes.start_point)::geometry) IN ( SELECT min(st_distancesphere((gc.lonlat)::geometry, (routes.start_point)::geometry)) AS min
             FROM gps_coordinates gc
            WHERE (gc.bus_id = buses.id))) AND (c_end.id >= c_start.id))
    ORDER BY c_start.id;
  SQL
  create_view "search_coordinates", sql_definition: <<-SQL
      SELECT DISTINCT ON (c_current.id) c_current.id,
      buses.plate,
      c_current.lonlat,
      c_current.created_at AS "current_date",
      (date_part('epoch'::text, (c_current.created_at - c_next.created_at)) / (60)::double precision) AS lapse
     FROM ((((gps_coordinates c_current
       JOIN buses ON ((c_current.bus_id = buses.id)))
       JOIN buses_routes ON ((buses_routes.bus_id = c_current.bus_id)))
       JOIN routes ON ((buses_routes.route_id = routes.id)))
       JOIN gps_coordinates c_next ON ((c_current.bus_id = c_next.bus_id)))
    WHERE ((st_distancesphere((c_next.lonlat)::geometry, (c_current.lonlat)::geometry) IN ( SELECT max(st_distancesphere((c_current.lonlat)::geometry, (gc.lonlat)::geometry)) AS max
             FROM gps_coordinates gc
            WHERE ((gc.bus_id = buses.id) AND (st_distancesphere((c_current.lonlat)::geometry, (gc.lonlat)::geometry) <= (100)::double precision)))) AND (c_next.id >= c_current.id));
  SQL
end

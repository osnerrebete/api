# == Schema Information
#
# Table name: search_buses
#
#  id         :bigint
#  end_date   :datetime
#  plate      :string
#  start_date :datetime
#  status     :integer
#  route_id   :bigint
#
class SearchCoordinateSerializer < ActiveModel::Serializer
  attributes :id, :status

  def status
    date_formatted = object.current_date.strftime("%I:%M %p")
    if object.lapse == 0
      "Paso por esta coordenada a las #{date_formatted} pero no se detuvo"
    else
      "Se detuvo en el lugar por #{object.lapse} minuto(s) a las #{date_formatted}"
    end
  end
end

# == Schema Information
#
# Table name: search_buses
#
#  id         :bigint
#  end_date   :datetime
#  plate      :string
#  start_date :datetime
#  status     :integer
#  route_id   :bigint
#
class SearchBusSerializer < ActiveModel::Serializer
  attributes :id, :plate, :route_id, :start_date, :end_date, :status

  def start_date
    object.start_date.strftime("%I:%M %p")
  end

  def end_date
    if object.status_before_type_cast == SearchBus.statuses[:started]
      return I18n.t "messages.unavaliable"
    end
    object.end_date.strftime("%I:%M %p")
  end

  def status
    SearchBus.human_attribute_name("status.#{object.status}")
  end
end

# { lapse == 0 ? `Pasó por esta coordenada a las ${current_date} pero no se detuvo` : }

# - Se detuvo en el lugar por 10 minutos a las 10:20 am
# -
# - No pasó por esta coordenada

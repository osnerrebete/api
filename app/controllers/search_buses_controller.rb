# frozen_string_literal: true

class SearchBusesController < ApplicationController
  def search
    @result = SearchBus.where(query).search_by_plate(search_params[:plate])
    render json: @result
  end

  private

  def search_params
    params.permit(:plate, :route_id, :date)
  end

  def query
    {
      route_id: search_params[:route_id],
      start_date: search_params[:date].to_time.all_day,
    }
  end
end

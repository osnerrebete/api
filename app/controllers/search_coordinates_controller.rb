# frozen_string_literal: true

class SearchCoordinatesController < ApplicationController
  def search
    @result = SearchCoordinate.search_by_coord(query)
    render json: @result
  end

  private

  def search_params
    params.permit(:coordinate, :plate)
  end

  def query
    {
      coordinate: search_params[:coordinate].gsub(" ", "").gsub(",", " "),
      plate: search_params[:plate],
    }
  end
end

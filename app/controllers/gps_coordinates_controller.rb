# frozen_string_literal: true

class GpsCoordinatesController < ApplicationController
  # POST /gps_coordinates
  def create
    lonlat = "POINT(#{coord_params[:latitude]} #{coord_params[:longitude]})"
    @gps_coordinate = GpsCoordinate.new(bus_id: bus.id, lonlat: lonlat)

    if @gps_coordinate.save
      render json: @gps_coordinate, status: :created
    else
      render json: @gps_coordinate.errors, status: :unprocessable_entity
    end
  end

  private

  def coord_params
    params.permit(:device_serial_number, :latitude, :longitude)
  end

  def bus
    Bus.find_by(gps_serial: coord_params[:device_serial_number])
  end
end

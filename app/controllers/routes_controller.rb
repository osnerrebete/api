# frozen_string_literal: true

class RoutesController < ApplicationController
  # GET /routes
  def index
    @routes = Route.all

    render json: @routes
  end
end

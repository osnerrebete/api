# frozen_string_literal: true

# == Schema Information
#
# Table name: routes
#
#  id          :bigint           not null, primary key
#  coordinates :geography        polygon, 4326
#  end_name    :string
#  end_point   :geography        point, 4326
#  start_name  :string
#  start_point :geography        point, 4326
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Route < ApplicationRecord
  has_and_belongs_to_many :buses
end

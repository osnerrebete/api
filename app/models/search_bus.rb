# == Schema Information
#
# Table name: search_buses
#
#  id         :bigint
#  end_date   :datetime
#  plate      :string
#  start_date :datetime
#  status     :integer
#  route_id   :bigint
#
class SearchBus < ApplicationRecord
  enum status: { started: 0, finished: 1, unfinished: 2, unknown: 3 }
  scope :search_by_plate, ->(plate) { where("plate ILIKE ?", "%#{plate}%") if plate.present? }

  def readonly?
    true
  end
end

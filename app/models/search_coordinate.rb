# == Schema Information
#
# Table name: search_coordinates
#
#  id           :bigint
#  current_date :datetime
#  lapse        :float
#  lonlat       :geography        point, 4326
#  plate        :string
#
class SearchCoordinate < ApplicationRecord
  scope :search_by_coord, ->(coordinate:, plate:) { where("ST_Equals(lonlat::geometry,st_geomfromtext(?,4326)) and plate ILIKE ?", "POINT(#{coordinate})", "%#{plate}%") }
end

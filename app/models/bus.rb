# frozen_string_literal: true

# == Schema Information
#
# Table name: buses
#
#  id         :bigint           not null, primary key
#  gps_serial :string
#  plate      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_buses_on_gps_serial  (gps_serial)
#  index_buses_on_plate       (plate)
#
class Bus < ApplicationRecord
  has_many :gps_coordinates
  has_and_belongs_to_many :routes
end

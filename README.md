## Descripción
El sistema está dividido en dos partes app (frontend) y api (backend):
- APP: está hecho en React y tiene dos componentes principales el header y el searchform (éste contiene el formulario de búsqueda, la tabla de resultados y el modal que permite buscar rutas usando coordenadas). El objetivo era crear una single-page application, es decir, una página única dónde se pudieran realizar la mayoría de las cosas
- API: está hecho en Rails 5, cree 4 controladores principales: 
    - gps_coordinates: aquí se tiene el endpoint que recibe datos del GPS y hace uso del modelo del mismo nombre para almacenar los datos
    - routes: lista todas las rutas que se tienen actualmente, comentaron que podían crecer en el tiempo así que cree una relación muchos a muchos con los buses, de manera que se puedan agregar en el futuro, asumí que las rutas ida y vuelta no necesariamente son iguales así que cree dos por defecto
    - search_buses: cree vistas para colocar allí las consultas complejas y usé la gema scenic para gestionar los archivos SQL, de manera que luego cree un modelo asociado a esas vistas y un controlador. Este controlador permite buscar los viajes de los buses enviando el id de la ruta, la fecha del viaje y la patente (opcional), y retorna los resultados correspondientes.
    - search_coordinates: este es similar al controlador anterior, la diferencia es que recibe la patente y la coordenada a buscar y retorna si estuvo allí, si se detuvo y si no pasó por esa coordenada.

## Cosas que me hubiese gustado mejorar/hacer:
- Me hubiese gustado probar más la búsqueda por coordenadas (search_coordinates) y hacer las mejoras pertinentes a la consulta
- Mejorar el diseño del front y hacer validaciones de los inputs en términos de usabilidad para prevenir los errores
- Realizar los tests correspondientes para back (hubiese utilizado Rspec para hacer las pruebas correspondientes) y front
- Realizar la funcionalidad de listar los eventos de salida/entrada de ruta
- Separar mejor los componentes de react para poder reutilizarlos en el futuro

## Versión de Ruby y Rails empleadas:
Ruby 2.6.3
Rails 5.2.4.1
### `rvm use 2.6.3`
### `gem install bundler`
### `bundle install`

## Configuración de la Base de Datos
Por favor editar el archivo de la configuración de la base de datos (Postgres), y agregar las credenciales correspondientes:

### `api/config/database.yml`

## Inicialización de la Base de Datos
Luego de configurar la base de datos, pueden ser utilizados los comandos de rails según sea necesario:

### `rake db:create`
### `rake db:migrate`
### `rake db:seed` 

Este último cargará algunos datos de pruebas, incluyendo algunas rutas.

## Iniciar el servidor
El servidor está configurado para utilizar el puerto 4000, ya que app funciona en el puerto 3000, y sólo se necesita usar el siguiente comando:
### `rails s`

## Simular un bus reportando cada 10s
### `rake gps:fake_data` 
Patente para hacer seguimiento: AABBCC

## Endpoint para enviar la geolocalización
### `http://localhost:4000/gps_coordinates` 

Estrucutra esperada (POST):
{
"device_serial_number": "d131dd02c5e6eec4",
"latitude": -70.69075584411621,
"longitude": -33.45382270754665
}

require 'test_helper'

class GpsCoordinatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gps_coordinate = gps_coordinates(:one)
  end

  test "should get index" do
    get gps_coordinates_url, as: :json
    assert_response :success
  end

  test "should create gps_coordinate" do
    assert_difference('GpsCoordinate.count') do
      post gps_coordinates_url, params: { gps_coordinate: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show gps_coordinate" do
    get gps_coordinate_url(@gps_coordinate), as: :json
    assert_response :success
  end

  test "should update gps_coordinate" do
    patch gps_coordinate_url(@gps_coordinate), params: { gps_coordinate: {  } }, as: :json
    assert_response 200
  end

  test "should destroy gps_coordinate" do
    assert_difference('GpsCoordinate.count', -1) do
      delete gps_coordinate_url(@gps_coordinate), as: :json
    end

    assert_response 204
  end
end

require 'test_helper'

class SearchBusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @search_bus = search_buses(:one)
  end

  test "should get index" do
    get search_buses_url, as: :json
    assert_response :success
  end

  test "should create search_bus" do
    assert_difference('SearchBus.count') do
      post search_buses_url, params: { search_bus: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show search_bus" do
    get search_bus_url(@search_bus), as: :json
    assert_response :success
  end

  test "should update search_bus" do
    patch search_bus_url(@search_bus), params: { search_bus: {  } }, as: :json
    assert_response 200
  end

  test "should destroy search_bus" do
    assert_difference('SearchBus.count', -1) do
      delete search_bus_url(@search_bus), as: :json
    end

    assert_response 204
  end
end
